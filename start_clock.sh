if [ -n "$SSH_CONNECTION" ]; then
        # remote session
        echo "remote login"
else
        # local login
        echo "local login, starting clock"
        sudo bash -c "echo none >/sys/class/leds/led0/trigger"
        cd ~/projects/SDLClock/bin/ARM/Release
        ./SDLClock.out
fi
