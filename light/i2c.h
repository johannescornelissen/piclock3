#pragma once

#include <string>

// i2c
#include <unistd.h>		
#include <fcntl.h>		
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>


class I2CDevice
{
public:
	I2CDevice(int aAddress, int aSMBUS)
	{
		fAddress = aAddress;
		fSMBUS = aSMBUS;
		fI2CFilehandle = -1;
	}
	virtual ~I2CDevice()
	{
		i2c_close();
	}
public:
	bool i2c_open(std::string aDevice)
	{
		i2c_close();
		fI2CFilehandle = open(aDevice.c_str(), O_RDWR);
		if (i2c_is_open())
		{
			if (ioctl(fI2CFilehandle, I2C_SLAVE, fAddress) < 0)
			{
				i2c_close();
				return false;
			}
			else
				return true;
		}
		return false;
	}
	bool i2c_is_open()
	{
		return fI2CFilehandle >= 0;
	}
	void i2c_close()
	{
		if (i2c_is_open())
		{
			close(fI2CFilehandle);
			fI2CFilehandle = -1;
		}
	}
	bool i2c_read_bytes(void* aBuffer, size_t aSize)
	{
		size_t readSize = read(fI2CFilehandle, aBuffer, aSize);
		return readSize == aSize;
	}
	bool i2c_read(uint8_t aRegister, void* aBuffer, size_t aSize)
	{
		return i2c_write_bytes(&aRegister, 1) ? i2c_read_bytes(aBuffer, aSize) : false;
	}
	bool i2c_write_bytes(void* aBuffer, size_t aSize = 2)
	{
		size_t writtenSize = write(fI2CFilehandle, aBuffer, aSize);
		return writtenSize == aSize;
	}
	bool i2c_write(uint8_t aRegister, uint8_t aValue)
	{
		uint8_t buffer[2]{ aRegister, aValue };
		return i2c_write_bytes(buffer, 2);
	}
	int getAddress() { return fAddress; }
	int getSMBUS() { return fSMBUS; }
	std::string getDevice() { return "/dev/i2c-" + std::to_string(getSMBUS()); }
private:
	int fI2CFilehandle;
	int fAddress;
	int fSMBUS;
};
