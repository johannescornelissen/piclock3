#pragma once

/* set access permissions more relax
sudo su - 
echo SUBSYSTEM==\"backlight\", RUN+=\"/bin/chmod 0666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power\" > /etc/udev/rules.d/99-backlight.rules
sudo reboot

max: echo 255 > /sys/class/backlight/rpi_backlight/brightness
echo 100 > /sys/class/backlight/rpi_backlight/brightness
min: echo 0 > /sys/class/backlight/rpi_backlight/brightness

off: echo 1 >/sys/class/backlight/rpi_backlight/bl_power
on:  echo 0 >/sys/class/backlight/rpi_backlight/bl_power
*/

/*
#include "i2c.h"
class RPI_BACKLIGHT : I2CDevice
{
public:
	RPI_BACKLIGHT() : I2CDevice(0x45, 1)
	{
		i2c_open(getDevice());
	}

	bool setBacklight(uint8_t aValue)
	{
		return i2c_write(0x86, aValue);
	}
};
*/

#include <fstream>

class RPI_BACKLIGHT
{
public:
	static bool setBrightness(int aBrightness)
	{
		if (fBrightness != aBrightness)
		{
			fBrightness = aBrightness;
			return write("/sys/class/backlight/rpi_backlight/brightness", std::to_string(aBrightness));
		}
		else
			return false;
	}
	static int getBrightness() 
	{ 
		return fBrightness; 
	}
	static bool setPower(bool aOn)
	{
		if (fIsPowerOn != aOn)
		{
			fIsPowerOn = aOn;
			return write("/sys/class/backlight/rpi_backlight/bl_power", aOn ? "0" : "1");
		}
		else
			return false;
	}
	static bool isPowerOn()
	{
		return fIsPowerOn;
	}
private:
	static bool write(std::string aPath, std::string aValue)
	{
		try
		{
			std::ofstream f(aPath);
			//f.write(aValue.c_str(), aValue.length());
			f << aValue << std::endl;
			f.close();
			return true;
		}
		catch (std::exception&)
		{
			return false;
		}
	}
	static inline bool fIsPowerOn = true; // mostly the screen would be on after a reboot
	static inline int fBrightness = 255; // mostly the screen would be at max brightness after a reboot
};