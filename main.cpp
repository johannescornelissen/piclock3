// todo: password login
// todo: password wifi (config-raspi?)
// todo: backlight parameter settable from file/env/cmdlin?

// rpi remote debugging
// sudo apt-get install openssh-server g++ gdb gdbserver

// sdl2
// https://gist.github.com/Lokathor/e6fec720b722b8a6f78e399698cae6e4

// sdl2 ttf
// https://www.raspberrypi.org/forums/viewtopic.php?t=248691
// rebuild freetype2
/*
mkdir $HOME/lib
wget https://download.savannah.gnu.org/releases/freetype/freetype-2.10.1.tar.gz
tar xfv freetype-2.10.1.tar.gz
cd freetype-2.10.1
./configure --enable-freetype-config --prefix=$HOME/lib/freetype2
make
make install
cd ..

cd SDL2_ttf-2.0.14
./configure --with-freetype-prefix=$HOME/lib/freetype2
make
sudo make install
*/

/*
retry with ttf: sudo apt-get install libsdl2-ttf-dev
but then r-make install SDL2, SDL2_image
*/

/*
sdl2 gfx
help from script https://github.com/neilmunday/pes/blob/master/setup/arch-rpi/install-sdl2.sh

wget http://www.ferzkopp.net/Software/SDL2_gfx/SDL2_gfx-1.0.4.tar.gz
tar zxvf SDL2_gfx-1.0.4.tar.gz
cd SDL2_gfx-1.0.4
./autogen.sh
./configure --prefix=/usr --host=arm-raspberry-linux-gnueabihf --disable-mmx
make
sudo make install
cd ..
*/

// 2020-04-25: retry SDL2
// https://solarianprogrammer.com/2015/01/22/raspberry-pi-raspbian-getting-started-sdl-2/

// untested extra info: https://discourse.libsdl.org/t/problems-using-sdl2-on-raspberry-pi-without-x11-running/22621/4

// enable/disable keyboard: http://www.gcat.org.uk/tech/?p=70

// tslib
// https://github.com/ImpulseAdventure/GUIslice/wiki/Install-tslib-on-Raspberry-Pi


/* disable scren blanking:
https://raspberrypi.stackexchange.com/questions/71572/screen-blanking-on-console-in-raspbian-stretch-debian-9
sudo su -
TERM=linux setterm -blank 0 >> /etc/issue
*/

// light sensor
/*
blue SDA
green SCL
yellow gnd
orange 3.3v
*/

#include <stdio.h>
#include <string>
#include <ctime>
#include <cmath>
#include <thread>
#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include <csignal>

#include "light/tsl2561.h"
#include "light/rpi_backlight.h"
#include "pid/pid.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "SDL2/SDL2_gfxPrimitives.h"

#include <tslib.h>

// rpi touch screen
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 480

// TTF uses SDL_Color (struct, use colorToSDLColor)
// GFX and rest of SDL uses 0xAABBGGRR

#define colorScreenBackground 0xFF000000
#define colorFlipBackground 0xFF404040 // 0xFF202020 // 0xFF101010
#define colorFlipCipher 0xFF909090 // 0xFFC8C8C8
#define colorFlipCipherLight 0xFFCCCCCC

#define colorHourDotOverlay 0xFF44FF00
#define colorMinuteDotOverlay 0x00DDDDDD // transparent for now
#define colorSecondDotOverlay 0xFFFF4400

#define INIT_ANIM_STEPS 20

#define BM_AUTO 0
#define BM_ALWAYS_ON 1
#define BM_ALWAYS_OFF 2
#define BM_NOTHING 3

#define SENSOR_LOOP_MS 200.0
#define PID_LUX_MAX_DELTA 10.0

#define DEFAULT_SENSOR_LUX_MAX 50.0
#define DEFAULT_SENSOR_LUX_MIN 1.0
#define DEFAULT_BACKLIGHT_MAX 255
#define DEFAULT_BACKLIGHT_MIN 10


double SENSOR_LUX_MAX = DEFAULT_SENSOR_LUX_MAX;
double SENSOR_LUX_MIN = DEFAULT_SENSOR_LUX_MIN;
int BACKLIGHT_MAX = DEFAULT_BACKLIGHT_MAX;
int BACKLIGHT_MIN = DEFAULT_BACKLIGHT_MIN;

int WATCH_FACE = 0;

#ifndef NDEBUG

// debug
#define pathResources "../../../"
//#define SHOW_FRAME_COUNT_AND_LUX
//#define SPED_UP_TIME
#define SHOW_PID_ON_CONSOLE
//#define SHOW_CLOCK_OVERLAY_DOTS
//#define SHOW_CLOCK_OVERLAY_CONSOLE
//#define DEBUG_TOUCHES

#else

// release
//#define pathResources ""
#define pathResources "../../../"

#endif // DEBUG


std::vector<std::string> DAY_OF_WEEK{ "ZO", "MA", "DI", "WO", "DO", "VR", "ZA" };
std::vector<std::string> MONTH{ "JAN", "FEB", "MAA", "APR", "MEI", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEC" };

SDL_Surface* CreateImageAlphaSurface(std::string aPath)
{
	auto loadedSurface = IMG_Load(aPath.c_str());
	return loadedSurface;
}

SDL_Texture* CreateImageTexture(std::string aPath, SDL_Renderer* aRenderer)
{
	auto loadedSurface = IMG_Load(aPath.c_str());
	if (loadedSurface == NULL)
	{
		std::cout << "## could not load texture " + aPath << std::endl;
		return NULL;
	}
	auto newTexture = SDL_CreateTextureFromSurface(aRenderer, loadedSurface);
	SDL_FreeSurface(loadedSurface);
	return newTexture;
}

SDL_Rect rectOffset(SDL_Rect* aRect, int dx, int dy)
{
	SDL_Rect rect{ aRect->x + dx, aRect->y + dy, aRect->w, aRect->h };
	return rect;
}

SDL_Rect rectInflate(SDL_Rect* aRect, int dx, int dy)
{
	SDL_Rect rect{ aRect->x - dx / 2, aRect->y - dy / 2, aRect->w + dx, aRect->h + dy };
	return rect;
}

void rectCenterX(SDL_Rect* aOuter, SDL_Rect* aInner)
{
	aInner->x = aOuter->x + (aOuter->w - aInner->w) / 2;
}

void rectCenterY(SDL_Rect* aOuter, SDL_Rect* aInner)
{
	aInner->y = aOuter->y + (aOuter->h - aInner->h) / 2;
}

void rectCenter(SDL_Rect* aOuter, SDL_Rect* aInner)
{
	aInner->x = aOuter->x + (aOuter->w - aInner->w) / 2;
	aInner->y = aOuter->y + (aOuter->h - aInner->h) / 2;
}

void rectGridCell(int aRowCount, int aColumnCount, double aRow, double aColumn, SDL_Rect* aOuter, SDL_Rect* aInner)
{
	aInner->w = aOuter->w / aColumnCount;
	aInner->h = aOuter->h / aRowCount;
	aInner->x = aOuter->x + (int)(aInner->w * aColumn);
	aInner->y = aOuter->y + (int)(aInner->h * aRow);
}

SDL_Color colorToSDLColor(Uint32 aColor)
{
	SDL_Color sdlColor;
	// aColor is 0xAABBGGRR
	sdlColor.a = (Uint8)((aColor >> 24) & 0xFF);
	sdlColor.b = (Uint8)((aColor >> 16) & 0xFF);
	sdlColor.g = (Uint8)((aColor >> 8) & 0xFF);
	sdlColor.r = (Uint8)((aColor >> 0) & 0xFF);
	return sdlColor;
}

void draw_flip_cipher(SDL_Renderer* aRenderer, SDL_Rect* aRect, TTF_Font* cipher_font, std::string cipher_text, Uint32 colorCipherText, Uint32 colorCipherBackground, Uint32 colorBackground)
{
	int line_width = std::max(aRect->h / 40, 1);
	int rr_radius = std::max(aRect->h / 10, 2);
	roundedBoxColor(aRenderer, (Sint16)aRect->x, (Sint16)aRect->y, (Sint16)(aRect->x + aRect->w), (Sint16)(aRect->y + aRect->h), (Sint16)rr_radius, colorCipherBackground);
	int glyphWidth;
	int glyphHeight;
	TTF_SizeText(cipher_font, cipher_text.c_str(), &glyphWidth, &glyphHeight);
	auto glyph = TTF_RenderText_Solid(cipher_font, cipher_text.c_str(), colorToSDLColor(colorCipherText));
	auto textureGlyph = SDL_CreateTextureFromSurface(aRenderer, glyph);
	SDL_FreeSurface(glyph);
	// zoom rect for glyph with 10% in height
	SDL_Rect rectGlyphDest = rectInflate(aRect, (int)(aRect->h * 0.35), (int)(aRect->h * 0.35));
	rectGlyphDest = rectOffset(&rectGlyphDest, 0, (int)(-aRect->h * 0.07));
	// keep aspect ratio of glyph
	rectGlyphDest.w = rectGlyphDest.h * glyphWidth / glyphHeight;
	// center in x
	rectCenterX(aRect, &rectGlyphDest);
	// render glyph
	SDL_RenderCopy(aRenderer, textureGlyph, NULL, &rectGlyphDest);
	SDL_DestroyTexture(textureGlyph);
	// render horizontal line as cut-out
	int y1 = aRect->y + aRect->h / 2 - line_width / 2;
	int y2 = y1 + line_width;
	for (int y = y1; y < y2; y++)
		hlineColor(aRenderer, (Sint16)aRect->x, (Sint16)(aRect->x + aRect->w), (Sint16)y, colorBackground);
}

constexpr double pi = 3.14159265358979323846;

void time_location(double aAngle, double aRadiusX, double aRadiusY, int aCenterX, int aCenterY, Uint16* x, Uint16* y)
{
	// calculate position on circle with given angle like 12 - hour analog clock
	*x = (Uint16)(aCenterX + std::sin(2.0 * pi * aAngle / 360.0) * aRadiusX);
	*y = (Uint16)(aCenterY - std::cos(2.0 * pi * aAngle / 360.0) * aRadiusY);
}

void RenderClockOverlay(SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter, double aHourAngle, double aMinuteAngle, double aSecondAngle)
{
#ifdef SHOW_CLOCK_OVERLAY_CONSOLE
	static int prevSecond = -1;
	int newSecond = (int)(60.0 * aSecondAngle / 360.0);
	if (prevSecond != newSecond)
	{
		printf("%3.1f  %3.1f  %d\n", 12.0 * aHourAngle / 360.0, 60.0 * aMinuteAngle / 360.0, newSecond);
		prevSecond = newSecond;
	}
#endif
#ifdef SHOW_CLOCK_OVERLAY_DOTS
	double radiusX = aRect->w / 2.05;
	double radiusY = aRect->h / 2.05;
	Uint16 x = 0;
	Uint16 y = 0;

	time_location(aHourAngle, radiusX, radiusY, aCenter->x, aCenter->y, &x, &y);
	filledCircleColor(aRenderer, x, y, (Sint16)(radiusX / 30), colorHourDotOverlay);

	time_location(aMinuteAngle, radiusX, radiusY, aCenter->x, aCenter->y, &x, &y);
	filledCircleColor(aRenderer, x, y, (Sint16)(radiusX / 35), colorMinuteDotOverlay);

	time_location(aSecondAngle, radiusX, radiusY, aCenter->x, aCenter->y, &x, &y);
	filledCircleColor(aRenderer, x, y, (Sint16)(radiusX / 40), colorSecondDotOverlay);
#endif
}

void RenderDate(SDL_Renderer* aRenderer, SDL_Rect* aRect, TTF_Font* aFont, int hour, int minuteA, int day, int month, int dayOfWeek)
{
	// background fill, needed because otherwise we get a white rectangle in all unused area!
	boxColor(aRenderer, (Sint16)(aRect->x), (Sint16)(aRect->y), (Sint16)(aRect->x + aRect->w), (Sint16)(aRect->y + aRect->h), colorScreenBackground);
	// render
	SDL_Rect cell;
	{
		// day-of-week
		rectGridCell(8, 6, 1, 2, aRect, &cell);
		SDL_Rect rect{ 0, 0, (int)(cell.h * 1.7), cell.h };
		rectCenter(&cell, &rect);
		draw_flip_cipher(aRenderer, &rect, aFont, DAY_OF_WEEK[dayOfWeek], colorFlipCipher, colorFlipBackground, colorScreenBackground);
	}
	{
		// day-of-month
		rectGridCell(8, 6, 1, 4, aRect, &cell);
		SDL_Rect rect{ 0, 0, (int)(cell.h * 1.1), cell.h };
		rectCenter(&cell, &rect);
		draw_flip_cipher(aRenderer, &rect, aFont, std::to_string(day), colorFlipCipher, colorFlipBackground, colorScreenBackground);
	}
	{
		// month
		rectGridCell(8, 6, 2.5, 3, aRect, &cell);
		SDL_Rect rect{ 0, 0, (int)(cell.h * 2.2), cell.h };
		rectCenter(&cell, &rect);
		draw_flip_cipher(aRenderer, &rect, aFont, MONTH[month], colorFlipCipher, colorFlipBackground, colorScreenBackground);
	}
	{
		// hour
		rectGridCell(8, 6, 5, 2 - 0.3, aRect, &cell);
		SDL_Rect rect{ 0, 0, (int)(cell.h * 2.1), (int)(cell.h * 2) };
		rectCenter(&cell, &rect);
		draw_flip_cipher(aRenderer, &rect, aFont, std::to_string(hour), colorFlipCipherLight, colorFlipBackground, colorScreenBackground);
	}
	{
		// minute
		rectGridCell(8, 6, 5, 4 + 0.2, aRect, &cell);
		SDL_Rect rect{ 0, 0, (int)(cell.h * 2.1), (int)(cell.h * 2) };
		rectCenter(&cell, &rect);
		std::string minuteStr = "0" + std::to_string(minuteA);
		minuteStr = minuteStr.substr(minuteStr.size() - 2);
		draw_flip_cipher(aRenderer, &rect, aFont, minuteStr, colorFlipCipherLight, colorFlipBackground, colorScreenBackground);
	}
}

void RenderFrameCountAndLux(SDL_Renderer* aRenderer, SDL_Rect* aRect, TTF_Font* aFont, int aFrameCount, double aLux)
{
#ifdef SHOW_FRAME_COUNT_AND_LUX
	SDL_Rect cell;
	{
		// frame count
		rectGridCell(8, 6, 4.5, 2, aRect, &cell);
		SDL_Rect rect{ 0, 0, cell.h * 3 / 2, cell.h }; // 3/2 square
		rectCenter(&cell, &rect);
		draw_flip_cipher(aRenderer, &rect, aFont, std::to_string(aFrameCount), colorFlipCipher, colorFlipBackground, colorScreenBackground);
	}
	{
		// lux
		rectGridCell(8, 6, 4.5, 4, aRect, &cell);
		SDL_Rect rect{ 0, 0, cell.h * 4 / 2, cell.h }; // 3/2 square
		rectCenter(&cell, &rect);
		draw_flip_cipher(aRenderer, &rect, aFont, std::to_string((int)aLux), colorFlipCipher, colorFlipBackground, colorScreenBackground);
	}
#endif
}

void RenderBacklightMode(SDL_Renderer* aRenderer, SDL_Rect* aRect, TTF_Font* aFont, int aBacklightMode)
{
	SDL_Rect cell;
	{
		// backlight mode
		rectGridCell(8, 6, 7, 3, aRect, &cell);
		SDL_Rect rect{ 0, 0, cell.h * 2, (int)(cell.h / 3) };
		rectCenter(&cell, &rect);
		std::string backlightMode;
		switch (aBacklightMode)
		{
			case BM_AUTO: backlightMode = "auto"; break;
			case BM_ALWAYS_ON: backlightMode = "always on"; break;
			case BM_ALWAYS_OFF: backlightMode = "always off"; break;
			default: backlightMode = "unknown"; break;
		}
		draw_flip_cipher(aRenderer, &rect, aFont, backlightMode, colorFlipCipher, colorFlipBackground, colorScreenBackground);
	}
}

void calculateArmAngles(int hour, int minuteA, int second, double* hourAngle, double* minuteAngle, double* secondAngle)
{
	double secondFraction = (double)second / 60.0;
	double minuteFraction = ((double)minuteA + secondFraction) / 60.0;
	double hourFraction = ((double)(hour % 12) + minuteFraction) / 12.0;
	*hourAngle = 360 * hourFraction;
	*minuteAngle = 360 * minuteFraction;
	*secondAngle = 360 * secondFraction;
}

class WatchFace
{
public:
	virtual ~WatchFace()
	{
		for (auto surface : fSurfaces)
			SDL_FreeSurface(surface);
		fSurfaces.clear();
		for (auto texture : fTextures)
			SDL_DestroyTexture(texture);
		fTextures.clear();
	}
public:
	virtual void Render(SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter, double aHourAngle, double aMinuteAngle, int aHour, int aMinute, int aSecond, bool aAnimations) = 0; // must override
	virtual void startNextAnimation() {}
	void rotateSequence(SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter,
		double aHourAngleFrom, double aHourAngleTo,
		double aMinuteAngleFrom, double aMinuteAngleTo,
		int aHour, int aMinute, int aSecond,
		int aAnimationTimeMS)
	{
		try
		{
			double currentHourAngle = aHourAngleFrom;
			double currentMinuteAngle = aMinuteAngleFrom;
			double deltaHourAngle = (aHourAngleTo - currentHourAngle) / (double)INIT_ANIM_STEPS;
			double deltaMinuteAngle = (aMinuteAngleTo - currentMinuteAngle) / (double)INIT_ANIM_STEPS;
			// animate
			int step = 0;
			while (step < INIT_ANIM_STEPS)
			{
				// render watch face
				SDL_RenderClear(aRenderer);
				Render(aRenderer, aRect, aCenter, currentHourAngle, currentMinuteAngle, aHour, aMinute, aSecond, false);
				SDL_RenderPresent(aRenderer);
				SDL_Delay(aAnimationTimeMS / INIT_ANIM_STEPS);
				step++;
				currentHourAngle += deltaHourAngle;
				currentMinuteAngle += deltaMinuteAngle;
			}
			// precise last step to avoid rounding errors
			SDL_RenderClear(aRenderer);
			Render(aRenderer, aRect, aCenter, aHourAngleTo, aMinuteAngleTo, aHour, aMinute, aSecond, false);
			SDL_RenderPresent(aRenderer);
		}
		catch (std::exception& e)
		{
			std::cout << "## exception in rotation animation: " << e.what() << std::endl;
		}
	}
	void rotateAnimation(SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter,double aHourAngle, double aMinuteAngle, int aHour, int aMinute, int aSecond)
	{
		bool hourClockWise = ((0 <= aHourAngle) && (aHourAngle < 90)) || ((180 <= aHourAngle) && (aHourAngle < 270));
		double destHourAngle = hourClockWise ? aHourAngle + 360 : aHourAngle - 360;
		double destMinuteAngle = hourClockWise ? aMinuteAngle - 360 : aMinuteAngle + 360;
		// do animation from given state to both arms at 6-o-clock and then back to current
		rotateSequence(aRenderer, aRect, aCenter, aHourAngle, destHourAngle, aMinuteAngle, destMinuteAngle, aHour, aMinute, aSecond, 2000);
		SDL_Delay(500);
		rotateSequence(aRenderer, aRect, aCenter, destHourAngle, aHourAngle, destMinuteAngle, aMinuteAngle, aHour, aMinute, aSecond, 2000);
	}
public:
	SDL_Surface* registerSurface(std::string aPath)
	{
		auto surface = CreateImageAlphaSurface(std::string(pathResources) + aPath);
		fSurfaces.push_back(surface);
		return surface;
	}
	SDL_Texture* registerTexture(std::string aPath, SDL_Renderer* aRenderer)
	{
		auto texture = CreateImageTexture(std::string(pathResources) + aPath, aRenderer);
		fTextures.push_back(texture);
		return texture;
	}
private:
	std::vector<SDL_Surface*> fSurfaces;
	std::vector<SDL_Texture*> fTextures;
};

class Station : public WatchFace
{
public:
	Station(SDL_Renderer* aRenderer)
	{
		surfaceDial = registerSurface("station/dial.png");
		textureHour = registerTexture("station/hour.png", aRenderer);
		textureMinute = registerTexture("station/min.png", aRenderer);
	}
public:
	virtual void Render(SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter, double aHourAngle, double aMinuteAngle, int aHour, int aMinute, int aSecond, bool aAnimations) override
	{
		if (rotate != 0)
		{
			rotate = 0;
			rotateAnimation(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aHour, aMinute, aSecond);
			// redraw for the last time to re-enter the normal render sequence
			Render(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aHour, aMinute, aSecond, false);
		}
		else
		{
			// create local temp texture
			SDL_Texture* textureBody = SDL_CreateTextureFromSurface(aRenderer, surfaceDial);
			// adjust color of body at lower backlight so arms stay relatiev better visible
			if (LowLight)
				SDL_SetTextureColorMod(textureBody, 192, 192, 192);
			SDL_RenderCopy(aRenderer, textureBody, NULL, aRect);
			SDL_RenderCopyEx(aRenderer, textureHour, NULL, aRect, aHourAngle, aCenter, SDL_FLIP_NONE);
			SDL_RenderCopyEx(aRenderer, textureMinute, NULL, aRect, aMinuteAngle, aCenter, SDL_FLIP_NONE);
			SDL_DestroyTexture(textureBody);
		}
	}
	virtual void startNextAnimation() override
	{
#ifndef NDEBUG
		std::cout << "starting animation " << nextAnimation << std::endl;
#endif
		// reset all animations
		rotate = 0;
		switch (nextAnimation)
		{
			default: rotate = 1; nextAnimation = 0; break;
		}
	}
public:
	bool LowLight = false;
private:
	// animation states
	size_t nextAnimation = 0;
	size_t rotate = 0;
	// texture references, not owned
	SDL_Surface* surfaceDial = nullptr;
	SDL_Texture* textureHour = nullptr;
	SDL_Texture* textureMinute = nullptr;
};

class Anitique : public WatchFace
{
public:
	Anitique(SDL_Renderer* aRenderer)
	{
		surfaceDial = registerSurface("antique/dial.png");
		textureHour = registerTexture("antique/hour.png", aRenderer);
		textureMinute = registerTexture("antique/min.png", aRenderer);
	}
public:
	virtual void Render(SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter, double aHourAngle, double aMinuteAngle, int aHour, int aMinute, int aSecond, bool aAnimations) override
	{
		if (rotate != 0)
		{
			rotate = 0;
			rotateAnimation(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aHour, aMinute, aSecond);
			// redraw for the last time to re-enter the normal render sequence
			Render(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aHour, aMinute, aSecond, false);
		}
		else
		{
			// create local temp texture
			SDL_Texture* textureBody = SDL_CreateTextureFromSurface(aRenderer, surfaceDial);
			// adjust color of body at lower backlight so arms stay relatiev better visible
			if (LowLight)
				SDL_SetTextureColorMod(textureBody, 192, 192, 192);
			SDL_RenderCopy(aRenderer, textureBody, NULL, aRect);
			SDL_RenderCopyEx(aRenderer, textureHour, NULL, aRect, aHourAngle, aCenter, SDL_FLIP_NONE);
			SDL_RenderCopyEx(aRenderer, textureMinute, NULL, aRect, aMinuteAngle, aCenter, SDL_FLIP_NONE);
			SDL_DestroyTexture(textureBody);
		}
	}
	virtual void startNextAnimation() override
	{
#ifndef NDEBUG
		std::cout << "starting animation " << nextAnimation << std::endl;
#endif
		// reset all animations
		rotate = 0;
		switch (nextAnimation)
		{
			default: rotate = 1; nextAnimation = 0; break;
		}
	}
public:
	bool LowLight = false;
private:
	// animation states
	size_t nextAnimation = 0;
	size_t rotate = 0;
	// texture references, not owned
	SDL_Surface* surfaceDial = nullptr;
	SDL_Texture* textureHour = nullptr;
	SDL_Texture* textureMinute = nullptr;
};

class Mickey1 : public WatchFace
{
public:
	Mickey1(SDL_Renderer* aRenderer)
	{
		surfaceDial = registerSurface("mickey1/dial.png");
		textureHour = registerTexture("mickey1/hour.png", aRenderer);
		textureMinute = registerTexture("mickey1/min.png", aRenderer);
	}
public:
	virtual void Render(SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter, double aHourAngle, double aMinuteAngle, int aHour, int aMinute, int aSecond, bool aAnimations) override
	{
		if (rotate != 0)
		{
			rotate = 0;
			rotateAnimation(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aHour, aMinute, aSecond);
			// redraw for the last time to re-enter the normal render sequence
			Render(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aHour, aMinute, aSecond, false);
		}
		else
		{
			// create local temp texture
			SDL_Texture* textureBody = SDL_CreateTextureFromSurface(aRenderer, surfaceDial);
			// adjust color of body at lower backlight so arms stay relatiev better visible
			if (LowLight)
				SDL_SetTextureColorMod(textureBody, 192, 192, 192);
			SDL_RenderCopy(aRenderer, textureBody, NULL, aRect);
			SDL_RenderCopyEx(aRenderer, textureHour, NULL, aRect, aHourAngle, aCenter, SDL_FLIP_NONE);
			SDL_RenderCopyEx(aRenderer, textureMinute, NULL, aRect, aMinuteAngle, aCenter, SDL_FLIP_NONE);
			SDL_DestroyTexture(textureBody);
		}
	}
	virtual void startNextAnimation() override
	{
#ifndef NDEBUG
		std::cout << "starting animation " << nextAnimation << std::endl;
#endif
		// reset all animations
		rotate = 0;
		switch (nextAnimation)
		{
			default: rotate = 1; nextAnimation = 0; break;
		}
	}
public:
	bool LowLight = false;
private:
	// animation states
	size_t nextAnimation = 0;
	size_t rotate = 0;
	// texture references, not owned
	SDL_Surface* surfaceDial = nullptr;
	SDL_Texture* textureHour = nullptr;
	SDL_Texture* textureMinute = nullptr;
};

class Mickey2Textures
{
public:
	SDL_Texture* background;
	SDL_Texture* scale;
	SDL_Texture* hour;
	SDL_Texture* body;

	std::vector<SDL_Texture*> sequenceTapShoe;

	SDL_Texture* head;

	std::vector<SDL_Texture*> sequenceBlinkEyes;

	SDL_Texture* minuteA;
	SDL_Texture* minuteB;
	double minuteAStartAngle;
	double minuteAEndAngle;

	std::vector<SDL_Texture*> sequenceYawnEyesAndMouth; // mouth is in eyes
	std::vector<SDL_Texture*> sequenceYawnHead;

	void render(
		SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter,
		double aHourAngle, double aMinuteAngle,
		SDL_Rect* aBodyRect, SDL_Rect* aFaceRect,
		size_t& blink, size_t& yawn, size_t& tap, size_t& dance)
	{
		SDL_RenderCopy(aRenderer, background, NULL, aRect);
		SDL_RenderCopy(aRenderer, scale, NULL, aRect);
		SDL_RenderCopyEx(aRenderer, hour, NULL, aRect, aHourAngle, aCenter, SDL_FLIP_NONE);
		SDL_RenderCopy(aRenderer, body, NULL, aBodyRect);
		// shoe (and sequence)
		SDL_RenderCopy(aRenderer, sequenceTapShoe[tap], NULL, aRect);
		if (tap > 0) { tap++; if (tap >= sequenceTapShoe.size()) { tap = 0; if (dance > 0) dance++; } }
		// head and eyes
		if (yawn == 0)
		{
			// optional blink
			SDL_RenderCopy(aRenderer, head, NULL, aFaceRect);
			SDL_RenderCopy(aRenderer, sequenceBlinkEyes[blink], NULL, aFaceRect);
			if (blink > 0) { blink++; if (blink >= sequenceBlinkEyes.size()) blink = 0; }
		}
		else
		{
			// yawn (sequence)
			size_t yawnDiv3 = (size_t)(yawn / 3);
			SDL_RenderCopy(aRenderer, sequenceYawnHead[yawnDiv3], NULL, aRect);
			SDL_RenderCopy(aRenderer, sequenceYawnEyesAndMouth[yawnDiv3], NULL, aRect);
			if (yawn > 0)
			{
				yawn++;
				if ((size_t)(yawn / 3) >= sequenceYawnHead.size())
					yawn = 0;
			}
		}
		// minute arm
		if (minuteAStartAngle <= aMinuteAngle && aMinuteAngle < minuteAEndAngle)
		{
			if (minuteA)
				SDL_RenderCopyEx(aRenderer, minuteA, NULL, aRect, aMinuteAngle, aCenter, SDL_FLIP_NONE);
		}
		else
		{
			if (minuteB)
				SDL_RenderCopyEx(aRenderer, minuteB, NULL, aRect, aMinuteAngle, aCenter, SDL_FLIP_NONE);
		}
	}
};

class Mickey2 : public WatchFace
{
public:
	Mickey2(SDL_Renderer* aRenderer)
	{

		auto textureBack = registerTexture("mickey2/back.png", aRenderer);
		auto textureScale = registerTexture("mickey2/scale1.png", aRenderer);
		{
			texturesA.background = textureBack;
			texturesA.scale = textureScale;
			texturesA.hour = registerTexture("mickey2/hour1.png", aRenderer);
			texturesA.body = registerTexture("mickey2/body1.png", aRenderer);

			auto shoeA = registerTexture("mickey2/shoe1.png", aRenderer);
			auto shoeB = registerTexture("mickey2/shoe1b.png", aRenderer);
			auto shoeC = registerTexture("mickey2/shoe1c.png", aRenderer);
			auto shoeD = registerTexture("mickey2/shoe1d.png", aRenderer);
			auto shoeE = registerTexture("mickey2/shoe1e.png", aRenderer);
			texturesA.sequenceTapShoe = std::vector<SDL_Texture*>{ shoeA, shoeB, shoeC, shoeD, shoeE, shoeD, shoeC, shoeB, shoeA };

			texturesA.head = registerTexture("mickey2/head1.png", aRenderer);

			auto eyesA = registerTexture("mickey2/eyes1.png", aRenderer);
			auto eyesB = registerTexture("mickey2/eyes1b.png", aRenderer);
			auto eyesC = registerTexture("mickey2/eyes1c.png", aRenderer);
			texturesA.sequenceBlinkEyes = std::vector<SDL_Texture*>{ eyesA, eyesB, eyesC, eyesB, eyesA };

			texturesA.minuteA = registerTexture("mickey2/minute1a.png", aRenderer);
			texturesA.minuteB = registerTexture("mickey2/minute1b.png", aRenderer);
			texturesA.minuteAStartAngle = 0;
			texturesA.minuteAEndAngle = 90;

			auto mouthYawnA = registerTexture("mickey2/mouthyawn1a.png", aRenderer);
			auto mouthYawnB = registerTexture("mickey2/mouthyawn1b.png", aRenderer);
			texturesA.sequenceYawnEyesAndMouth = std::vector<SDL_Texture*>{
				eyesA, eyesB, eyesC,
				mouthYawnA, mouthYawnA, mouthYawnB, mouthYawnB, mouthYawnB, mouthYawnB, mouthYawnA,
				eyesC, eyesB, eyesA
			};

			auto headA = texturesA.head;
			auto headYawnA = registerTexture("mickey2/headyawn1a.png", aRenderer);
			auto headYawnB = registerTexture("mickey2/headyawn1b.png", aRenderer);
			texturesA.sequenceYawnHead = std::vector<SDL_Texture*>{
				headA, headA, headA,
				headYawnA, headYawnA, headYawnB, headYawnB, headYawnB, headYawnB, headYawnA,
				headA, headA, headA
			};
		}
		{
			texturesB.background = textureBack;
			texturesB.scale = textureScale;
			texturesB.hour = registerTexture("mickey2/hour2.png", aRenderer);
			texturesB.body = registerTexture("mickey2/body2.png", aRenderer);

			auto shoeA = registerTexture("mickey2/shoe2.png", aRenderer);
			auto shoeB = registerTexture("mickey2/shoe2b.png", aRenderer);
			auto shoeC = registerTexture("mickey2/shoe2c.png", aRenderer);
			auto shoeD = registerTexture("mickey2/shoe2d.png", aRenderer);
			auto shoeE = registerTexture("mickey2/shoe2e.png", aRenderer);
			texturesB.sequenceTapShoe = std::vector<SDL_Texture*>{ shoeA, shoeB, shoeC, shoeD, shoeE, shoeD, shoeC, shoeB, shoeA };

			texturesB.head = registerTexture("mickey2/head2.png", aRenderer);

			auto eyesA = registerTexture("mickey2/eyes2.png", aRenderer);
			auto eyesB = registerTexture("mickey2/eyes2b.png", aRenderer);
			auto eyesC = registerTexture("mickey2/eyes2c.png", aRenderer);
			texturesB.sequenceBlinkEyes = std::vector<SDL_Texture*>{ eyesA, eyesB, eyesC, eyesB, eyesA };

			texturesB.minuteA = registerTexture("mickey2/minute2a.png", aRenderer);
			texturesB.minuteB = registerTexture("mickey2/minute2b.png", aRenderer);
			texturesB.minuteAStartAngle = 180;
			texturesB.minuteAEndAngle = 225;

			auto mouthYawnA = registerTexture("mickey2/mouthyawn2a.png", aRenderer);
			auto mouthYawnB = registerTexture("mickey2/mouthyawn2b.png", aRenderer);
			texturesB.sequenceYawnEyesAndMouth = std::vector<SDL_Texture*>{
				eyesA, eyesB, eyesC,
				mouthYawnA, mouthYawnA, mouthYawnB, mouthYawnB, mouthYawnB, mouthYawnB, mouthYawnA,
				eyesC, eyesB, eyesA
			};

			auto headA = texturesB.head;
			auto headYawnA = registerTexture("mickey2/headyawn2a.png", aRenderer);
			auto headYawnB = registerTexture("mickey2/headyawn2b.png", aRenderer);
			texturesB.sequenceYawnHead = std::vector<SDL_Texture*>{
				headA, headA, headA,
				headYawnA, headYawnA, headYawnB, headYawnB, headYawnB, headYawnB, headYawnA,
				headA, headA, headA
			};
		}
		{
			// textures for wave animation, base is texturesA
			texturesWave.background = textureBack;
			texturesWave.scale = textureScale;
			texturesWave.hour = registerTexture("mickey2/arms3.png", aRenderer);
			texturesWave.body = texturesA.body;

			texturesWave.sequenceTapShoe = texturesA.sequenceTapShoe;

			texturesWave.head = registerTexture("mickey2/head3.png", aRenderer);

			auto eyesA = registerTexture("mickey2/eyes3.png", aRenderer);
			auto eyesB = registerTexture("mickey2/eyes3b.png", aRenderer);
			auto eyesC = registerTexture("mickey2/eyes3c.png", aRenderer);
			texturesWave.sequenceBlinkEyes = std::vector<SDL_Texture*>{ eyesA, eyesB, eyesC, eyesB, eyesA };

			// no minute arm
			texturesWave.minuteA = nullptr;
			texturesWave.minuteB = nullptr;
			texturesA.minuteAStartAngle = 0;
			texturesA.minuteAEndAngle = 90;

			texturesWave.sequenceYawnEyesAndMouth = std::vector<SDL_Texture*>{
				eyesA
			};

			auto headA = texturesWave.head;
			texturesWave.sequenceYawnHead = std::vector<SDL_Texture*>{
				headA,
			};

			auto handsA = registerTexture("mickey2/hands3.png", aRenderer);
			auto handsB = registerTexture("mickey2/hands3b.png", aRenderer);
			auto handsC = registerTexture("mickey2/hands3c.png", aRenderer);
			fSequenceWaveHands = std::vector<SDL_Texture*>{
				handsA, handsB, handsA, handsC,
				handsA, handsB, handsA, handsC,
				handsA, handsB, handsA, handsC,
				handsA, handsB, handsA, handsC
			};
		}

		fSequenceBody = std::vector<SDL_Point>{ {0,0}, {1,4}, {3,8}, {6,12}, {3,16}, {0,20}, {-2,16},{-6,12}, {-3,8},{-1,4}, {0,0} };
	}
public:
	virtual void Render(SDL_Renderer* aRenderer, SDL_Rect* aRect, SDL_Point* aCenter, double aHourAngle, double aMinuteAngle, int aHour, int aMinute, int aSecond, bool aAnimations) override
	{
		if (rotate != 0)
		{
			rotate = 0;
			rotateAnimation(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aHour, aMinute, aSecond);
			// redraw for the last time to re-enter the normal render sequence
			size_t dummy = 0;
			getTextures(aHourAngle)->render(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aRect, aRect, dummy, dummy, dummy, dummy);
		}
		else if (wave != 0)
		{
			wave = 0;
			// show animation of mickey waving
			// arms go to 50 deg and 310 deg
			rotateSequence(aRenderer, aRect, aCenter, aHourAngle, aHourAngle < 180 ? 50 : 310, aMinuteAngle, aHourAngle < 180 ? 310 : 50, aHour, aMinute, aSecond, 1000);
			SDL_Delay(100);
			// wave hands
			size_t blink = 0;
			for (size_t sequence = 0; sequence < fSequenceWaveHands.size(); sequence++)
			{
				SDL_RenderClear(aRenderer);
				size_t dummy = 0;
				// blink half way during wave
				if (sequence == (size_t)(fSequenceWaveHands.size() / 2))
					blink = 1;
				texturesWave.render(aRenderer, aRect, aCenter, 0, 0, aRect, aRect, blink, dummy, dummy, dummy);
				SDL_RenderCopy(aRenderer, fSequenceWaveHands[sequence], NULL, aRect);
				SDL_RenderPresent(aRenderer);
				SDL_Delay(sequence == 0 ? 200 : 100);
			}
			SDL_Delay(100);
			// rotate arms back to original positions
			rotateSequence(aRenderer, aRect, aCenter, aHourAngle < 180 ? 50 : 310, aHourAngle, aHourAngle < 180 ? 310 : 50, aMinuteAngle, aHour, aMinute, aSecond, 1000);
			SDL_Delay(100);
			// redraw for the last time to re-enter the normal render sequence
			{
				size_t dummy = 0;
				getTextures(aHourAngle)->render(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, aRect, aRect, dummy, dummy, dummy, dummy);
			}
		}
		else
		{
			// pre-calc
			// dance sequence
			if (dance > 0 && body == 0 && tap == 0)
			{
				switch (dance)
				{
					case 1: body = 1; tap = 1; break;
					case 2: body = 1; break;
					case 3: body = 1; tap = 1; break;
					case 4: body = 1; break;
					case 5: tap = 1; break;
					case 6: tap = 1; break;
					default: dance = 0; break;
				}
			}
			SDL_Rect stretchBodyRect = *aRect;
			SDL_Rect stretchFaceRect = *aRect;
			if (body > 0)
			{
				SDL_Point bodyDelta = fSequenceBody[body];
				// adjust body rect
				stretchBodyRect.x += bodyDelta.x;
				stretchBodyRect.w -= bodyDelta.x;
				stretchBodyRect.y += bodyDelta.y;
				stretchBodyRect.h -= bodyDelta.y;
				// adjust face rect
				stretchFaceRect.y += bodyDelta.y / 2;
				stretchFaceRect.h -= bodyDelta.y / 2;
				// next in sequence
				body++;
				if (body >= fSequenceBody.size()) { body = 0; if (dance > 0) dance++; }
			}
			// check to start animations
			if (aAnimations)
			{
				if (prev_second != aSecond)
				{
					auto minuteOfDay = aHour * 60 + aMinute;
					// blink every 7 seconds
					if ((yawn == 0) && (blink == 0) && (aSecond % 7 == 0))
						blink = 1;
					// dance every hour for 20 seconds
					if ((yawn == 0) && (dance == 0) && (aMinute == 0) && ((0 <= aSecond) && (aSecond < 20)))
						dance = 1;
					// tap every 15 minute exception on the hour (dance)
					if ((yawn == 0) && (dance == 0) && (aMinute % 15 == 0) && (0 == aSecond))
						tap = 1;
					// yawn every 13 minutes between 20:00 and 8:00
					if ((dance == 0) && (blink == 0) && ((20 <= aHour) || (aHour < 8)) && (aMinute % 13 == 0) && ((aSecond == 0) || (aSecond == 4)))
						yawn = 1;
					// wave every 43 minutes
					if ((blink == 0) && (dance == 0) && (yawn == 0) && (minuteOfDay % 43 == 0) && (aSecond == 0))
						wave = 1;
					prev_second = aSecond;
				}
			}
			// render
			getTextures(aHourAngle)->render(aRenderer, aRect, aCenter, aHourAngle, aMinuteAngle, &stretchBodyRect, &stretchFaceRect, blink, yawn, tap, dance);
		}
	}
	Mickey2Textures* getTextures(double aHourAngle)
	{
		double hourAngle = std::fmod(aHourAngle, 360.0);
		return (0.0 <= hourAngle) && (hourAngle < 180.0) ? &texturesB : &texturesA;
	}
	virtual void startNextAnimation() override
	{
#ifndef NDEBUG
		std::cout << "starting animation " << nextAnimation << std::endl;
#endif
		// reset all animations
		blink = 0; tap = 0; body = 0; dance = 0; yawn = 0; rotate = 0; wave = 0;
		switch (nextAnimation)
		{
			case 0:  blink = 1; nextAnimation++; break;
			case 1:  tap = 1; nextAnimation++; break;
			case 2:  body = 1; nextAnimation++; break;
			case 3:  dance = 1; nextAnimation++; break;
			case 4:  yawn = 1; nextAnimation++; break;
			case 5:  rotate = 1; nextAnimation++; break;
			default: wave = 1; nextAnimation = 0; break;
		}
	}
private:
	int prev_second = -1;
	// animation states
	size_t nextAnimation = 0;
	size_t blink = 0;
	size_t tap = 0;
	size_t body = 0;
	size_t dance = 0;
	size_t yawn = 0;
	size_t rotate = 0;
	size_t wave = 0;
	std::vector<SDL_Point> fSequenceBody;
	std::vector<SDL_Texture*> fSequenceWaveHands;
	// textures, references
	Mickey2Textures texturesA; // 1 looks to his right (6-12 o'clock)
	Mickey2Textures texturesB; // 2 looks to his left (12-6 o'clock)
	Mickey2Textures texturesWave; // 3 looks to the front (wave, body etc. is from 1)
};

class TouchAction
{
public:
	TouchAction(SDL_Rect aRect, std::function<void(Uint64 aDuratioMS)> aActionTouchEnd, std::string aDescription)
	{
		rect = aRect;
		actionTouchEnd = aActionTouchEnd;
		description = aDescription;
	}
	SDL_Rect rect;
	std::function<void(Uint64 aDuratioMS)> actionTouchEnd;
	std::string description;
};

double luxToBacklight(double aLux)
{
	// lux ~ brightness seems to work ok between 0-100 -> 10-255
	// y = a(x-c) + b
	// c = SENSOR_LUX_MIN
	// b = BACKLIGHT_MIN
	// a = (y-b)/(x-c) = (BACKLIGHT_MAX-BACKLIGHT_MIN)/(SENSOR_LUX_MAX-SENSOR_LUX_MIN)
	double c = SENSOR_LUX_MIN;
	double b = BACKLIGHT_MIN;
	double a = (BACKLIGHT_MAX - BACKLIGHT_MIN) / (SENSOR_LUX_MAX - SENSOR_LUX_MIN);
	return std::max((double)BACKLIGHT_MIN, std::min((double)BACKLIGHT_MAX, a * (aLux - c) + b));
}

void getCurrentTime(int* hour, int* minuteA, int* second, int* day, int* month, int* year, int* dayOfWeek)
{
	auto time = std::time(NULL);
	auto localTime = std::localtime(&time);
	*second = localTime->tm_sec;
	*minuteA = localTime->tm_min;
	*hour = localTime->tm_hour;
	*day = localTime->tm_mday;
	*month = localTime->tm_mon;
	*year = localTime->tm_year;
	*dayOfWeek = localTime->tm_wday; // 0..6
}

void readOrWriteConfig()
{
	std::ifstream configFile;
	configFile.open(pathResources "SDLClock.config", std::ios::in);
	if (configFile.is_open())
	{
		std::string line;
		while (std::getline(configFile, line))
		{
			try
			{
				size_t sep = line.find('=');
				if (sep != std::string::npos)
				{
					std::string name = line.substr(0, sep);
					std::string value = line.substr(sep + 1);
					if (name == "SENSOR_LUX_MAX")
						SENSOR_LUX_MAX = (double)std::stoi(value);
					if (name == "SENSOR_LUX_MIN")
						SENSOR_LUX_MIN = (double)std::stoi(value);
					if (name == "BACKLIGHT_MAX")
						BACKLIGHT_MAX = std::stoi(value);
					if (name == "BACKLIGHT_MIN")
						BACKLIGHT_MIN = std::stoi(value);
					if (name == "WATCH_FACE")
						WATCH_FACE = std::stoi(value);
				}
			}
			catch (std::exception& e)
			{
				std::cout << "## exception reading config line " << line << ": " << e.what() << std::endl;
			}
		}
		configFile.close();
	}
	else
	{
		// no config read so write a new one with the defaults
		std::ofstream newConfigFile;
		newConfigFile.open(pathResources "SDLClock.config", std::ios::out | std::ios::app);
		newConfigFile << "# range of lux values of light sensor to map on display brightness (default: " << DEFAULT_SENSOR_LUX_MIN << ".." << DEFAULT_SENSOR_LUX_MAX << ")" << std::endl;
		newConfigFile << "SENSOR_LUX_MAX" << "=" << std::to_string((int)SENSOR_LUX_MAX) << std::endl;
		newConfigFile << "SENSOR_LUX_MIN" << "=" << std::to_string((int)SENSOR_LUX_MIN) << std::endl;
		newConfigFile << "# backlight brightness, valid range 0..255 (default: " << DEFAULT_BACKLIGHT_MIN << ".." << DEFAULT_BACKLIGHT_MAX << ")" << std::endl;
		newConfigFile << "BACKLIGHT_MAX" << "=" << std::to_string(BACKLIGHT_MAX) << std::endl;
		newConfigFile << "BACKLIGHT_MIN" << "=" << std::to_string(BACKLIGHT_MIN) << std::endl;
		newConfigFile << "# watch face: 0 mickey2 (default), 1 mickey1, 2 station, 3 antique" << std::endl;
		newConfigFile << "WATCH_FACE" << "=" << std::to_string(WATCH_FACE) << std::endl;
		newConfigFile.close();
	}
#ifndef NDEBUG
	std::cout << "starting with backlight settings" << std::endl;
	std::cout << "   sensor (lux): " << SENSOR_LUX_MIN << " - " << SENSOR_LUX_MAX << std::endl;
	std::cout << "   backlight (0-255): " << BACKLIGHT_MIN << " - " << BACKLIGHT_MAX << std::endl;
	std::cout << "   watch face (0..3): " << WATCH_FACE << std::endl;
#endif
}

int quit = -1;

int main(int argc, char* args[])
{
	// handler for sig term
	std::signal(SIGTERM, [](int signal) -> void
		{
			std::cout << "## SIGTERM: " << signal << std::endl;
			quit = 0;
		});

	// SDL init
	SDL_Window* window = NULL;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
		return 1;
	}
	window = SDL_CreateWindow("Clock", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (window == NULL)
	{
		fprintf(stderr, "could not create window: %s\n", SDL_GetError());
		return 1;
	}
	// hide mouse
	SDL_ShowCursor(SDL_DISABLE);
	//Initialize PNG loading
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		SDL_DestroyWindow(window);
		SDL_Quit();
		fprintf(stderr, "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
		return 2;
	}
	// init SDL ttf
	if (TTF_Init() < 0)
	{
		IMG_Quit();
		SDL_DestroyWindow(window);
		SDL_Quit();
		fprintf(stderr, "could not initialize sdl_ttf Error: %s\n", TTF_GetError());
		return 3;
	}
	auto font = TTF_OpenFont(pathResources "fonts/seguisb.ttf", 200);
	if (font == nullptr)
	{
		TTF_Quit();
		IMG_Quit();
		SDL_DestroyWindow(window);
		SDL_Quit();
		fprintf(stderr, "could not load font: %s\n", TTF_GetError());
		return 4;
	}
	auto renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	// viewable screen dimensions in mm 155 x 86
	double pixelDimensionRatio = (155.0 / SCREEN_WIDTH) / (86.0 / SCREEN_HEIGHT);
	SDL_Rect watchFaceRect{ 0,0,(int)std::round(SCREEN_HEIGHT / pixelDimensionRatio) ,SCREEN_HEIGHT }; // left most square (in dimension, not pixels)
	SDL_Point watchFaceCenter{ watchFaceRect.w / 2 ,watchFaceRect.h / 2 };
	SDL_Rect dateRect{ watchFaceRect.x + watchFaceRect.w , watchFaceRect.y, SCREEN_WIDTH - watchFaceRect.w, watchFaceRect.h };

	// sensor and backlight config
	readOrWriteConfig();

	// reset brightness so we start on a defined value
	RPI_BACKLIGHT::setBrightness(BACKLIGHT_MIN);

	int backlight_mode = BM_AUTO;
	PID pid = PID(SENSOR_LOOP_MS / 1000.0, PID_LUX_MAX_DELTA, -PID_LUX_MAX_DELTA, 0.7, 0.0, 0.005);// 0.1, 0.5, 0.01);
	RPI_BACKLIGHT::setPower(true);
	TSL2561 lightSensor = TSL2561(SENSOR_LOOP_MS);
	double pidLux = 60;
	lightSensor.onValue = [&pid, &pidLux, &backlight_mode](auto aSensorLux) -> void
	{
		RPI_BACKLIGHT::setPower((backlight_mode != BM_ALWAYS_OFF) && ((aSensorLux != 0) || (backlight_mode == BM_ALWAYS_ON)));
		if (backlight_mode == BM_AUTO && aSensorLux < SENSOR_LUX_MAX)
		{
			if (SENSOR_LUX_MIN < aSensorLux)
			{
				double inc = pid.calculate(aSensorLux, pidLux);
				pidLux += inc;
				int brightness = (int)luxToBacklight(pidLux);
				if (RPI_BACKLIGHT::setBrightness(brightness))
				{
#ifdef SHOW_PID_ON_CONSOLE
					std::cout << "lux: " << aSensorLux << ", pid: " << pidLux << " -> brightness: " << brightness << std::endl;
#endif
				}
			}
			else
			{
				if (RPI_BACKLIGHT::setBrightness(BACKLIGHT_MIN))
				{
#ifdef SHOW_PID_ON_CONSOLE
					std::cout << "lux: " << aSensorLux << " set min brightness: " << BACKLIGHT_MIN << std::endl;
#endif
				}
			}
		}
		else
		{
			if (RPI_BACKLIGHT::setBrightness(BACKLIGHT_MAX))
			{
#ifdef SHOW_PID_ON_CONSOLE
				std::cout << "lux: " << aSensorLux << " set max brightness: " << BACKLIGHT_MAX << std::endl;
#endif
			}
		}
	};
	lightSensor.start();
	{
		// create watch face
		std::shared_ptr<WatchFace> watchface;
		switch (WATCH_FACE)
		{
			case 1:
				watchface = std::make_shared<Mickey1>(renderer);
				break;
			case 2:
				watchface = std::make_shared<Station>(renderer);
				break;
			case 3:
				watchface = std::make_shared<Anitique>(renderer);
				break;
			default:
				watchface = std::make_shared<Mickey2>(renderer);
				break;
		}

		// init touch event handler
		// define touch actions
		std::vector<TouchAction> touchActions;

		SDL_Rect touchAnimRect = watchFaceRect;
		touchAnimRect.y = watchFaceRect.y + (int)(watchFaceRect.h * 0.8);
		touchAnimRect.h = (int)(watchFaceRect.h * (1.0 - 0.8));
		touchActions.push_back(TouchAction(touchAnimRect, [&watchface](auto aDuratioMS)-> void
			{
				if (aDuratioMS < 3000)
					watchface->startNextAnimation();
				else
				{
					std::cout << ">> quit: touch time " << aDuratioMS << std::endl;
					quit = 5;
				}
			}, "touched watch face"));

		SDL_Rect touchPowerRect = dateRect;
		touchPowerRect.y = dateRect.y + (int)(dateRect.h * 0.8);
		touchPowerRect.h = (int)(dateRect.h * (1.0 - 0.8));
		touchActions.push_back(TouchAction(touchPowerRect, [&backlight_mode](auto aDuratioMS)-> void
			{
				backlight_mode++;
				if (backlight_mode >= BM_NOTHING)
					backlight_mode = 0;
#ifndef NDEBUG
				if (backlight_mode == BM_ALWAYS_OFF)
					std::cout << ">> backlight is switched off" << std::endl;
				else
					std::cout << ">> backlight is switched on" << std::endl;
#endif
			}, "touched date"));

		// start touch handler thread
		struct tsdev* ts = nullptr;
		std::thread touchEventThread = std::thread([&ts, &touchActions]() -> void
			{
#define SLOTS 5
#define SAMPLES 1
				char* tsdevice = NULL;
				struct ts_sample_mt TouchScreenSamples[SAMPLES][SLOTS];
				struct ts_sample_mt(*pTouchScreenSamples)[SLOTS] = TouchScreenSamples;
				int ret, i, j;
				time_t touchStart = 0;
				suseconds_t touchStartu = 0;
				ts = ts_setup(tsdevice, 0);
				if (ts)
				{
					struct ts_sample_mt* ts_samp[SAMPLES];
					for (i = 0; i < SAMPLES; i++)
						ts_samp[i] = pTouchScreenSamples[i];
					while (ts && (quit < 0))
					{
						try
						{
							ret = ts_read_mt(ts, ts_samp, SLOTS, SAMPLES);
							if (ret >= 0)
							{
								for (j = 0; j < ret; j++)
								{
									for (i = 0; i < SLOTS; i++)
									{
										if (ts_samp[j][i].valid)
										{
#ifdef DEBUG_TOUCHES
											printf("%ld.%06ld: (slot %d) %6d %6d %6d\n", ts_samp[j][i].tv.tv_sec, ts_samp[j][i].tv.tv_usec, ts_samp[j][i].slot,
												ts_samp[j][i].x, ts_samp[j][i].y, ts_samp[j][i].pressure);
#endif
											if (ts_samp[j][i].pressure == 0)
											{
												// end of touch
												SDL_Point point{ ts_samp[j][i].x , ts_samp[j][i].y };
												bool handled = false;
												auto delta_ms = (ts_samp[j][i].tv.tv_sec - touchStart) * 1000 + (ts_samp[j][i].tv.tv_usec - touchStartu) / 1000;
												for (auto& action : touchActions)
												{
													if (SDL_PointInRect(&point, &action.rect))
													{
#ifndef NDEBUG
														std::cout << "   touch action " << action.description << ": " << delta_ms << std::endl;
#endif
														action.actionTouchEnd(delta_ms);
														handled = true;
													}
												}
												if (!handled)
												{
#ifndef NDEBUG
													std::cout << ">> no handler found for end touch " << ts_samp[j][0].x << ", " << ts_samp[j][0].y << ": " << delta_ms << std::endl;
#endif
												}
												touchStart = 0;
											}
											else
											{
												if (touchStart == 0)
												{
													touchStart = ts_samp[j][i].tv.tv_sec;
													touchStartu = ts_samp[j][i].tv.tv_usec;
												}
											}
										}
									}
								}
							}
						}
						catch (std::exception& e)
						{
							std::cout << "## exception in touch handler: " << e.what() << std::endl;
						}
					}
					ts_close(ts);
				}
			});


		// date/time
		int hour = 0;
		int minuteA = 0;
		int second = 0;
		int day = 0;
		int month = 0;
		int year = 0;
		int dayOfWeek = 0;
		double hourAngle;
		double minuteAngle;
		double secondAngle;
		// frame/s
		int frameCount = 0;
		int frameCounter = 0;
		int frameCountSecond = 0;
		// do first animation to go to current time
		{
			getCurrentTime(&hour, &minuteA, &second, &day, &month, &year, &dayOfWeek);
			calculateArmAngles(hour, minuteA, second, &hourAngle, &minuteAngle, &secondAngle);
			// start a-round 6 o'clock
			watchface->rotateSequence(renderer, &watchFaceRect, &watchFaceCenter, hourAngle < 180 ? 179 : 181, hourAngle, minuteAngle < 180 ? 179 : 181, minuteAngle, hour, minuteA, second, 1000);
		}
		// loop
		SDL_Event e;
		while (quit < 0)
		{
			try
			{
				//Handle events on queue
				if (SDL_WaitEventTimeout(&e, 20)) // small timeout for fluid animations
				{
					do
					{
						//User requests quit
						if (e.type == SDL_QUIT)
						{
							quit = 0;
							std::cout << "## SDL_QUIT" << std::endl;
						}
						else if (e.type == SDL_KEYDOWN)
						{
							switch (e.key.keysym.sym)
							{
								case SDLK_ESCAPE:
								case SDLK_q:
									quit = 0;
									std::cout << "## key q or escape: " << e.key.keysym.sym << std::endl;
									break;
							}
						}
					} while (SDL_PollEvent(&e));
				}
				if (quit < 0)
				{
#ifdef SPED_UP_TIME
					// counter for sped up time
					int dummy = 0;
					getCurrentTime(&dummy, &dummy, &second, &day, &month, &year, &dayOfWeek);
					minute += 1;
					if (minute > 59)
					{
						hour += 1;
						minute = 0;
						if (hour > 23)
							hour = 0;
					}
#else
					// normal time
					getCurrentTime(&hour, &minuteA, &second, &day, &month, &year, &dayOfWeek);
#endif
					// frame counter: target 15-20 frames/s ?
					if (frameCountSecond != second)
					{
						// store and reset frame count
						frameCount = frameCounter;
						frameCounter = 0;
						frameCountSecond = second;
					}
					else
						frameCounter++;
					calculateArmAngles(hour, minuteA, second, &hourAngle, &minuteAngle, &secondAngle);
					// render watch face
					SDL_RenderClear(renderer);
					watchface->Render(renderer, &watchFaceRect, &watchFaceCenter, hourAngle, minuteAngle, hour, minuteA, second, true);
					RenderClockOverlay(renderer, &watchFaceRect, &watchFaceCenter, hourAngle, minuteAngle, secondAngle);
					RenderDate(renderer, &dateRect, font, hour, minuteA, day, month, dayOfWeek);
					RenderFrameCountAndLux(renderer, &dateRect, font, frameCount, lightSensor.lux());
					RenderBacklightMode(renderer, &dateRect, font, backlight_mode);
					SDL_RenderPresent(renderer);
				}
			}
			catch (std::exception& e)
			{
				std::cout << "## exception in main loop (" << quit << "): " << e.what() << std::endl;
			}
		}
		// stop touch event handler
		/*
		if (ts)
		{
			ts_close(ts);
			ts = nullptr;
		}
		if (touchEventThread.joinable())
			touchEventThread.join();
		*/
	}
	// cleanup
	lightSensor.stop();
	TTF_CloseFont(font);
	TTF_Quit();
	IMG_Quit();
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window); // and screenSurface
	SDL_Quit();
	return quit;
}