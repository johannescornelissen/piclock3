/**
 * Copyright 2019 Bradley J. Snyder <snyder.bradleyj@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

class PID
{
public:
    // Kp -  proportional gain
    // Ki -  Integral gain
    // Kd -  derivative gain
    // dt -  loop interval time
    // max - maximum value of manipulated variable
    // min - minimum value of manipulated variable
        
    PID(double aDt, double aMax, double aMin, double aP, double aI, double aD)
    {
        fDt = aDt;
        fMax = aMax; 
        fMin = aMin; 
        fP = aP; 
        fI = aI;
        fD = aD;
    }
    double calculate(double aSetpoint, double aPreviousValue)
    {
        // Calculate error
        double error = aSetpoint - aPreviousValue;
        // Proportional term
        double Pout = fP * error;
        // Integral term
        fIntegral += error * fDt;
        double Iout = fI * fIntegral;
        // Derivative term
        double derivative = (error - fPreviousError) / fDt;
        double Dout = fD * derivative;
        // Calculate total output
        double output = Pout + Iout + Dout;
        // Restrict to max/min
        if (output > fMax)
            output = fMax;
        else if (output < fMin)
            output = fMin;
        // Save error to previous error
        fPreviousError = error;
        return output;
    }
private:
    double fDt;
    double fMax;
    double fMin;
    double fP;
    double fD;
    double fI;
    double fPreviousError;
    double fIntegral;
};
